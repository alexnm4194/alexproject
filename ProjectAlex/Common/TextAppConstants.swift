//
//  TextAppConstants.swift
//  ProjectAlex
//
//  Created by Axity Noriega on 05/05/20.
//  Copyright © 2020 Alejandro Noriega. All rights reserved.
//

import Foundation
import UIKit

struct URLS {
    static let login = "https://reqres.in/api/login"
    static let users = "https://reqres.in/api/users?page=2"
}

struct LoginText {
    static let EmailTitle = "Correo electrónico"
    static let PasswordTitle = "Contraseña"
    static let LoginButtonTitle = "Entrar"
    static let alertLoginTitle = "Alerta"
    static let alertLoginMessage = "El correo o la contraseña son incorrectos"
    static let alertLoginOk = "Aceptar"
}

struct HomeText {
    static let listTitle = "Usuarios"
}

struct DetailText {
    static let nameTitle = "Nombre"
    static let lastNameTitle = "Apellido"
    static let emailTitle = "Email"
}
