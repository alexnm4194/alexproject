//
//  LoginViewController.swift
//  ProjectAlex
//
//  Created by Axity Noriega on 05/05/20.
//  Copyright © 2020 Alejandro Noriega. All rights reserved.
//
import UIKit
import Alamofire

class LoginViewController: ViewControllerBase<LoginView> {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        View.delegate = self
        self.navigationController?.isNavigationBarHidden = true
        
    }
       
}

// MARK: - LoginViewDelegate
extension LoginViewController: LoginViewDelegate {
    
    func checkUser(_ view: LoginView, didTapLoginButton data: LoginParameters) {
                
        if (!data.email.isEmpty && !data.password.isEmpty){
            self.showLoader(onView: View)
            
            AF.request(URLS.login, method: .post, parameters: data)
            .responseJSON { response in
                switch (response.result) {
                case .success:
                    self.validUser(response: response.response!)
                    
                case .failure(let error):
                    self.removeLoader()
                    if error._code == NSURLErrorTimedOut {
                        debugPrint("Request timeout!")
                    } else {
                        debugPrint(response)
                    }
                }
            }
        }
    }

}

// MARK: - LoginView extension
extension LoginViewController {
    
    func validUser(response: AnyObject) {
        self.removeLoader()
        
        if (response.statusCode == 200) {
            let home = HomeViewController()
            self.navigationController?.setViewControllers([home], animated: true)
        } else {
            let alert = UIAlertController(title: LoginText.alertLoginTitle, message: LoginText.alertLoginMessage, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: LoginText.alertLoginOk, style: .default, handler: { _ in self.View.clearData() }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
}
