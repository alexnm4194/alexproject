//
//  Login.swift
//  ProjectAlex
//
//  Created by Axity Noriega on 05/05/20.
//  Copyright © 2020 Alejandro Noriega. All rights reserved.
//

import Foundation
import UIKit

protocol LoginViewDelegate: class {
    func checkUser(_ view: LoginView, didTapLoginButton data: LoginParameters)
}

struct LoginParameters: Codable {
    var email: String
    var password: String
}
