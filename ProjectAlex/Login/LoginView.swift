//
//  LoginView.swift
//  ProjectAlex
//
//  Created by Axity Noriega on 05/05/20.
//  Copyright © 2020 Alejandro Noriega. All rights reserved.
//

import UIKit

class LoginView: ViewBase {
    weak var delegate: LoginViewDelegate?
    
    private lazy var backGroundImage: UIImageView = {
        let background = UIImage(named: "fondoLogin")

        let imageView : UIImageView!
        imageView = UIImageView(frame: self.bounds)
        imageView.contentMode =  UIView.ContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = self.center
        
        return imageView
    }()
    
    private lazy var blurView: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        
        return UIVisualEffectView(effect: blurEffect)
    }()
    
    private lazy var image: UIImageView = {
        let imageView  = UIImageView()
        imageView.image = UIImage(named:"login")
       
        return imageView
    }()
    
    private lazy var emailTextField: UITextField = {
        let textField =  UITextField()
        textField.attributedPlaceholder = NSAttributedString(string: LoginText.EmailTitle,
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        textField.backgroundColor = UIColor(white: 1, alpha: 0.0)
        textField.font = UIFont.systemFont(ofSize: 15)
        textField.textColor = .white
        textField.borderStyle = UITextField.BorderStyle.roundedRect
        textField.autocorrectionType = UITextAutocorrectionType.no
        textField.keyboardType = UIKeyboardType.emailAddress
        textField.returnKeyType = UIReturnKeyType.continue
        textField.clearButtonMode = UITextField.ViewMode.whileEditing
        textField.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        textField.delegate = self
        
        return textField
    }()
    
    private lazy var passwordTextField: UITextField = {
        let textField =  UITextField()
        textField.isSecureTextEntry = true
        textField.attributedPlaceholder = NSAttributedString(string: LoginText.PasswordTitle,
        attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        textField.backgroundColor = UIColor(white: 1, alpha: 0.0)
        textField.font = UIFont.systemFont(ofSize: 15)
        textField.textColor = .white
        textField.borderStyle = UITextField.BorderStyle.roundedRect
        textField.autocorrectionType = UITextAutocorrectionType.no
        textField.keyboardType = UIKeyboardType.default
        textField.returnKeyType = UIReturnKeyType.done
        textField.clearButtonMode = UITextField.ViewMode.whileEditing
        textField.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        textField.delegate = self
        
        return textField
    }()
    
    private lazy var loginButton: UIButton = {
        let button =  UIButton()
        button.backgroundColor = .lightGray
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1
        button.setTitle(LoginText.LoginButtonTitle, for: .normal)
        
        return button
    }()
    
    override func setViews() {
        super.setViews()
        
        loginButton.addTarget(self, action: #selector(didTapLoginButton(_:)), for: .touchUpInside)
        
        addSubview(backGroundImage)
        addSubview(blurView)
        addSubview(image)
        addSubview(emailTextField)
        addSubview(passwordTextField)
        addSubview(loginButton)
    }
    
    override func setlayout() {
        addConstraintsWithFormat(format: "H:|[v0]|", views: backGroundImage)
        addConstraintsWithFormat(format: "V:|[v0]|", views: backGroundImage)
        addConstraintsWithFormat(format: "H:|[v0]|", views: blurView)
        addConstraintsWithFormat(format: "V:|[v0]|", views: blurView)
        
        addConstraintsWithFormat(format: "H:[v0(250)]", views: image)
        addConstraintsWithFormat(format: "H:|-20-[v0]-20-|", views: emailTextField)
        addConstraintsWithFormat(format: "H:|-20-[v0]-20-|", views: passwordTextField)
        addConstraintsWithFormat(format: "H:|-20-[v0]-20-|", views: loginButton)
        
        addConstraintsWithFormat(format: "V:[v0(250)]-80-[v1(50)]-20-[v2(50)]-50-[v3(50)]-150-|", views: image, emailTextField, passwordTextField, loginButton)
        
        image.translatesAutoresizingMaskIntoConstraints = false
        let horizontalConstraint = image.centerXAnchor.constraint(equalTo: self.centerXAnchor)
        NSLayoutConstraint.activate([horizontalConstraint])
    }
    
}

// MARK: - Button action
private extension LoginView {

    @objc func didTapLoginButton(_ button: UIButton) {
        let data = LoginParameters.init(email: emailTextField.text ?? "", password: passwordTextField.text ?? "")
        
        delegate?.checkUser(self, didTapLoginButton: data)
        endEditing(false)
    }

}

extension LoginView {
    func clearData() {
        passwordTextField.text = ""
    }
}

// MARK: - TextField Delegate
extension LoginView: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == emailTextField {
            textField.resignFirstResponder()
            passwordTextField.becomeFirstResponder()

            if self.frame.origin.y == 0 {
                self.frame.origin.y -= 70
            }
        } else if textField == passwordTextField {
            textField.resignFirstResponder()
            self.frame.origin.y = 0
        }

        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == passwordTextField {
            if self.frame.origin.y == 0 {
                self.frame.origin.y -= 70
            }
        }
        
        return true
    }
    
}
