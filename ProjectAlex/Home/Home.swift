//
//  Home.swift
//  ProjectAlex
//
//  Created by Axity Noriega on 05/05/20.
//  Copyright © 2020 Alejandro Noriega. All rights reserved.
//

import Foundation
import UIKit

protocol HomeViewDelegate: class {
    func tapUser(_ view: HomeView, didTapUser index: Int)
}

struct UsersResponse: Decodable {
    let id: Int
    let email: String
    let first_name: String
    let last_name: String
    let avatar: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case email
        case first_name
        case last_name
        case avatar
    }
}

struct ListUsersResponse: Decodable {
  let all: [UsersResponse]
  
  enum CodingKeys: String, CodingKey {
    case all = "data"
  }
}
