//
//  HomeViewController.swift
//  ProjectAlex
//
//  Created by Axity Noriega on 05/05/20.
//  Copyright © 2020 Alejandro Noriega. All rights reserved.
//

import UIKit
import Alamofire

class HomeViewController: ViewControllerBase<HomeView> {
    
    lazy var usersArray = [UsersResponse]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.title = HomeText.listTitle
        View.delegate = self
        self.getUsers()
    }
    
    func getUsers() {
        AF.request(URLS.users, method: .get)
        .responseDecodable(of: ListUsersResponse.self) { (response) in
            
          guard let users = response.value else { return }
            self.usersArray = users.all
            let names = users.all.map({ $0.first_name + " " + $0.last_name })
            self.View.configData(users: names)
        }
    }

}

// MARK: - LoginViewDelegate
extension HomeViewController: HomeViewDelegate {
    
    func tapUser(_ view: HomeView, didTapUser index: Int) {
        debugPrint(self.usersArray[index])
        
        let detail = DetailViewController()
        self.navigationController?.pushViewController(detail, animated: true)
        detail.configLabels(user: self.usersArray[index])
    }

}
