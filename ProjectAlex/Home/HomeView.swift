//
//  HomeView.swift
//  ProjectAlex
//
//  Created by Axity Noriega on 05/05/20.
//  Copyright © 2020 Alejandro Noriega. All rights reserved.
//

import UIKit

class HomeView: ViewBase {
    private var myArray = [""]
    weak var delegate: HomeViewDelegate?
    
    private lazy var backGroundImage: UIImageView = {
        let background = UIImage(named: "madrid")

        let imageView : UIImageView!
        imageView = UIImageView(frame: self.bounds)
        imageView.contentMode =  UIView.ContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = self.center
        
        return imageView
    }()
    
    private lazy var tableUsers: UITableView = {
        let table = UITableView()
        table.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        table.backgroundColor = UIColor(white: 1, alpha: 0.0)
        table.dataSource = self
        table.delegate = self
        
        return table
    }()
    
    override func setViews() {
        super.setViews()
        
        addSubview(backGroundImage)
        addSubview(tableUsers)
    }
    
    override func setlayout() {
        addConstraintsWithFormat(format: "H:|[v0]|", views: backGroundImage)
        addConstraintsWithFormat(format: "V:|[v0]|", views: backGroundImage)
        
        addConstraintsWithFormat(format: "H:|[v0]|", views: tableUsers)
        addConstraintsWithFormat(format: "V:|[v0]|", views: tableUsers)
    }
    
}

extension HomeView: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.tapUser(self, didTapUser: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath as IndexPath)
        cell.textLabel!.text = "\(myArray[indexPath.row])"
        cell.backgroundColor = UIColor(white: 1, alpha: 0.0)
        
        let customColorView = UIView()
        customColorView.backgroundColor = UIColor(white: 1, alpha: 0.3)
        cell.selectedBackgroundView = customColorView
        cell.accessoryType = .disclosureIndicator
        
        return cell
    }
}

extension HomeView {
    
    func configData(users: [String]) {
        myArray = users
        tableUsers.reloadData()
    }
    
}
