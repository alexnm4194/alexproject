//
//  ViewControllerBase.swift
//  ProjectAlex
//
//  Created by Axity Noriega on 05/05/20.
//  Copyright © 2020 Alejandro Noriega. All rights reserved.
//

import UIKit

class ViewControllerBase<V: ViewBase>: UIViewController {

    override func loadView() {
        view = V()
    }

    var View: V {
        return view as! V
    }

}
