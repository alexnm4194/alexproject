//
//  ViewBase.swift
//  ProjectAlex
//
//  Created by Axity Noriega on 05/05/20.
//  Copyright © 2020 Alejandro Noriega. All rights reserved.
//

import UIKit

class ViewBase: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)

        setViews()
        setlayout()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        setViews()
        setlayout()
    }

    /// Set your view and its subviews here.
    func setViews() {
        backgroundColor = .white
    }

    /// Layout your subviews here.
    func setlayout() {}

}

extension ViewBase {
    
    func addConstraintsWithFormat(format: String, views: UIView?...) {
        var viewsDictionary = [String:UIView]()
        for (index,view) in views.enumerated()
        {
            let key = "v\(index)"
            viewsDictionary[key] = view
            view!.translatesAutoresizingMaskIntoConstraints = false
        }
        
        //For xcode 10 use the next line
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewsDictionary))
    }
}
