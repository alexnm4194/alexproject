//
//  DetailViewController.swift
//  ProjectAlex
//
//  Created by Axity Noriega on 06/05/20.
//  Copyright © 2020 Alejandro Noriega. All rights reserved.
//

import UIKit

class DetailViewController: ViewControllerBase<DetailView> {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func configLabels(user: UsersResponse) {
        self.navigationItem.title = user.first_name + " " + user.last_name
        View.configLabels(user: user)
    }
    
}
