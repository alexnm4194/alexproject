//
//  DetailView.swift
//  ProjectAlex
//
//  Created by Axity Noriega on 06/05/20.
//  Copyright © 2020 Alejandro Noriega. All rights reserved.
//

import UIKit

class DetailView: ViewBase {
    
    private lazy var image: UIImageView = {
        let imageView  = UIImageView()
        imageView.image = UIImage(named:"login")
       
        return imageView
    }()
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.text = DetailText.nameTitle
        label.textColor = .black
        label.backgroundColor = .white
        label.font = UIFont.systemFont(ofSize: 18)
        
        return label
    }()
    
    private lazy var userNameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.text = ""
        label.textColor = .lightGray
        label.backgroundColor = .white
        label.font = UIFont.systemFont(ofSize: 16)
        
        return label
    }()
    
    private lazy var lastNameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.text = DetailText.lastNameTitle
        label.textColor = .black
        label.backgroundColor = .white
        label.font = UIFont.systemFont(ofSize: 18)
        
        return label
    }()
    
    private lazy var userLastNameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.text = ""
        label.textColor = .lightGray
        label.backgroundColor = .white
        label.font = UIFont.systemFont(ofSize: 16)
        
        return label
    }()
    
    private lazy var emailLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.text = DetailText.emailTitle
        label.textColor = .black
        label.backgroundColor = .white
        label.font = UIFont.systemFont(ofSize: 18)
        
        return label
    }()
    
    private lazy var userEmailLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.text = ""
        label.textColor = .lightGray
        label.backgroundColor = .white
        label.font = UIFont.systemFont(ofSize: 16)
        
        return label
    }()
    
    override func setViews() {
        super.setViews()
        
        addSubview(image)
        addSubview(nameLabel)
        addSubview(userNameLabel)
        addSubview(lastNameLabel)
        addSubview(userLastNameLabel)
        addSubview(emailLabel)
        addSubview(userEmailLabel)

    }
    
    override func setlayout() {
        
        addConstraintsWithFormat(format: "H:[v0(250)]", views: image)
        addConstraintsWithFormat(format: "H:|-20-[v0]-20-|", views: nameLabel)
        addConstraintsWithFormat(format: "H:|-20-[v0]-20-|", views: userNameLabel)
        addConstraintsWithFormat(format: "H:|-20-[v0]-20-|", views: lastNameLabel)
        addConstraintsWithFormat(format: "H:|-20-[v0]-20-|", views: userLastNameLabel)
        addConstraintsWithFormat(format: "H:|-20-[v0]-20-|", views: emailLabel)
        addConstraintsWithFormat(format: "H:|-20-[v0]-20-|", views: userEmailLabel)
        
        addConstraintsWithFormat(format: "V:|-200-[v0(250)]-20-[v1(30)]-10-[v2(30)]-10-[v3(30)]-10-[v4(30)]-10-[v5(30)]-10-[v6(30)]", views: image, nameLabel, userNameLabel, lastNameLabel, userLastNameLabel, emailLabel, userEmailLabel)
        
        image.translatesAutoresizingMaskIntoConstraints = false
        let horizontalConstraint = image.centerXAnchor.constraint(equalTo: self.centerXAnchor)
        NSLayoutConstraint.activate([horizontalConstraint])
    }
    
}

// MARK: - extension DetailView
extension DetailView {
    
    func configLabels(user: UsersResponse) {
        userNameLabel.text = user.first_name
        userLastNameLabel.text = user.last_name
        userEmailLabel.text = user.email
        
        self.load(url: URL(string: user.avatar)!)
    }
    
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let imag = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self!.image.image = imag
                    }
                }
            }
        }
    }
    
}
